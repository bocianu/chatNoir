;******************************************************************************** 
;                                                        
;    chatnoir                                             
;    anticShop
;    bocianu@gmail.com
;
;******************************************************************************** 

;*************************************  INCLUDES

 icl 'atari.hea'

;debug_mode = 1                             ; uncomment this line to get on screen debug info
 
;*************************************  DEFINES

VERSION_H           equ "0"
VERSION_Lh          equ "0"
VERSION_Ll          equ "1"


JOY_SKIP_TIME       equ 1                   ; timeout (in vsync cycles) between cursor jumps, when joy keeped in one direction
KEY_SKIP_TIME       equ 16                   ; timeout (in vsync cycles) between console keypresses


GAME_BG_COLOR       equ $82

FIELD_H             equ 48
FIELD_W             equ 256

SPEED_MAX_X         equ 16
SPEED_MAX_Y         equ 32

;*************************************  ZERO PAGE GLOBAL VARIABLES
            org $80
src             .wo 0                       ; source addres
dest            .wo 0                       ; destination addres
dataw           .wo 0                       
boardPos        .wo 0 
cx              .by 0                       ; and other useful variables
cy              .by 0
sx              .by 0
sy              .by 0
vx              .by 0
vy              .by 0
viewX           .wo 0
viewY           .wo 0
joyTimer        .by JOY_SKIP_TIME           
pos_offset      .by 0
speed_x         .by 0
dir_x           .by 0
speed_y         .by 0
dir_y           .by 0


;*************************************  VBI                      **************

            org $600
vblk                                      

            lda speed_x
            :3 lsr
            sta sx
            beq @+
            lda dir_x
            cmp #0
            beq xleft
xright      lda viewX
            clc
            adc sx
            sta viewX
            scc
            inc viewX+1 
            cpw viewX #4*(FIELD_W-40)
            scc 
            mwa #4*(FIELD_W-40) viewX

            jmp @+
xleft       lda viewX
            sec
            sbc sx     
            sta viewX
            scs
            dec viewX+1
            lda viewX+1
            spl
            mwa #0 viewX

            
@           lda speed_y
            :3 lsr
            sta sy
            beq update_scroll
            lda dir_y
            beq yleft
yright      lda viewY
            clc
            adc sy
            sta viewY
            scc
            inc viewY+1 
            cpw viewY #16*(FIELD_H-10)
            scc
            mwa #16*(FIELD_H-10) viewY
            
            jmp update_scroll
yleft       lda viewY
            sec
            sbc sy     
            sta viewY
            scs
            dec viewY+1
            lda viewY+1
            spl
            mwa #0 viewY




update_scroll 

            lda viewX
            
            and #3
            eor #15
            clc 
            sta hscrol
            
            lda viewY
            and #15
            sta vscrol

update_view 

            lda viewX
            :2 lsr
            sta vx
            lda viewX+1
            :6 asl
            ora vx
            sta dest
            
            lda viewY
            :4 lsr
            sta vy
            lda viewY+1
            :4 asl
            ora vy
            sta dest+1

            mwa #vram_game src
            adw src dest src
            ldx #11
            ldy #0
            mwa #dl_view+1 dest 
@           
            lda src
            sta (dest),y
            iny
            lda src+1
            sta (dest),y
            :2 iny
            adw src #$100
            dex
            bne @-
        

endvblk           
            jmp sysvbv            


;*************************************  GLOBAL VARIABLES
            org $2000

joyPos          .by 15
joyNow          .by 15
joyFire         .by 1


;*************************************  MAIN                     **************

main
            cld
            

initGame    ; ****** game initialization (screen and data)
            

            mva #JOY_SKIP_TIME joyTimer 

            mva #0 vx
            mva #0 vy
            mva #0 sx
            mva #0 sy
            mwa #0 viewX
            mwa #0 viewY
            
            lda #$06    
            ldx >vblk ;HIGH BYTE OF USER ROUTINE
            ldy <vblk ;LOW BYTE
            jsr setvbv
            
            jsr scrInitGame
            jsr initPMG
@           lda strig0  ; wait for fire up
            beq @-

            mva #$ff keycode      ; clear char buffer
            
main_loop   ; ****** main Game loop
        
            lda:cmp:req 20      ; wait 4 VSYNC
            
            lda keycode         ; read keyboard
            and #%00111111
            cmp #28             ; esc pressed
            bne @+
            nop 
@            
            lda stick0          ; Check if stick moved
            eor #15  
            bne s_move 
            jsr idleStick
            jmp @+           
s_move      jsr movedStick
@           
            lda strig0          ; check FIRE button
            cmp joyFire
            seq 
            jsr pressedFire
            
            jmp main_loop 

            

;*************************************  CONTROLLER ROUTINES

escPressed  


            jmp main_loop

idleStick 
            lda speed_x
            cmp #0
            beq @+
            dec speed_x
@           lda speed_y
            cmp #0
            beq @+
            dec speed_y
@
            rts
            
movedStick  ; ******* Checkin stick position, if you know what I mean ;)
            
            sta joyNow
            cmp #15 
            sne 
            jmp check_out
            lsr                 ; check bit 0001 - up
            bcc @+
            
            lda dir_y
            bne backY 

            lda speed_y
            cmp #SPEED_MAX_Y
            bpl @+1
            inc speed_y
            inc speed_y
            inc speed_y
            inc speed_y
            jmp @+1

@           lda joyNow           
            lsr:lsr                 ; check bit 0010 - down
            bcc @+

            lda dir_y
            beq backY 

            lda speed_y
            cmp #SPEED_MAX_Y
            bpl @+
            inc speed_y
            inc speed_y
            inc speed_y
            inc speed_y
           
            jmp @+
backY       lda speed_y
            beq switchY
            dec speed_y
         
            jmp @+            
            
switchY     lda dir_y
            eor #1
            sta dir_y        


                        
@           lda joyNow
            :3 lsr                 ; check bit 0100 - left
            bcc @+

            lda dir_x
            bne backX 

            lda speed_x
            cmp #SPEED_MAX_X
            bpl @+1
            inc speed_x
            inc speed_x
            inc speed_x
            inc speed_x
            jmp @+1



@           lda joyNow
            :4 lsr                 ; check bit 1000 - right
            bcc @+

            lda dir_x
            beq backX 

            lda speed_x
            cmp #SPEED_MAX_X
            bpl @+
            inc speed_x
            inc speed_x
            inc speed_x
            inc speed_x
            jmp @+

backX       lda speed_x
            beq switchX
            dec speed_x
         
            jmp @+            
            
switchX     lda dir_x
            eor #1
            sta dir_x        



@           jmp check_out           

                       
check_out       
            rts            

pressedFire ; ****** Check if fire pressed down
            sta joyFire                 ; store previous state
            cmp #1
            sne
            jmp button_up

                        
button_up   
            rts

 
 icl 'screen.asm'
 
            run main
 