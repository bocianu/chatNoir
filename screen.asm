

outdec8s    ; ****** outputs 8bit decimal number without leading zeroes
            ; ****** stolen and adapted from: 
            ; ****** http://6502org.wikidot.com/software-output-decimal
            ldx #1
            stx c
            inx
            ldy #$40
o1          sty b
            lsr
o2          rol
            bcs o3
            cmp a,x
            bcc o4
o3          sbc a,x
            sec
o4          rol b
            bcc o2
            tay
            cpx c
            lda b
            bcc o5
            beq o6
            stx c

o5          ora #$10
            jsr output
o6          tya
            ldy #$10
            dex
            bpl o1
            rts
        
a .db  128,160,200
b .ds  1
c .ds  1

output      sty cy        
            ldy pos_offset
            sta (dest),y
            inc pos_offset
            ldy cy 
            rts

printNum    ; *** prints 8bit numer from accumulator to position at dest
            mvy #0 pos_offset
            jsr outdec8s
            ldy pos_offset
            lda #0
            sta (dest),y
            rts


;*************************************  SCREEN ROUTINES

scrInitGame
            mva #0 sdmctl       ; turn off antic
            mva #$40 nmien
            mwa #dlist_game sdlstl   ; set Display List
            mwa #dli_game vdslst     ; set Display List interrupt
//            mva >charset_game 756    ; set charset
            lda:cmp:req 20      ; wait 4 VSYNC
            mva #$c0 nmien
            mva #34 sdmctl      ; turn on antic
            
            mva #$80 710          ; set colors
            mva #GAME_BG_COLOR 712          ; set colors
            mva #$a 709          ; set colors
            mva #0 708          ; set colors
            
           
            rts            



;*************************************  DISPLAY LISTS INTERRUPTS

.align $100
dli_game    pha:txa:pha 
            
            pla:tax:pla
            rti

;*************************************  PLAYER MISSLE GRAPHICS
            
initPMG     ; ****** Initializes Playes/Missle graphics
            mva #@dmactl(standard|dma|players|lineX2) sdmctl
            mva >pmg pmbase     ; missiles and players data address
            ldy #0              ; clearing pmg memory
            lda #0 
clrPMG      :4 sta  pmg+$200+#*$80,y 
            iny
            bpl clrPMG
            
            mva #17 gprior       ; set priority
            mva #3 pmcntl       ; enable players and missles
            
            mva #0 sizem        ; clear PMG initial params
            
            :4 sta sizep:1
            :4 sta hposp:1
            :4 sta colpm:1
            :4 sta hposm:1
            
//            mva #CURSOR_COLOR_LIGHT colpm0     ; set PMG colors
//            sta colpm2
            //mva #CURSOR_COLOR_DARK colpm1
            //sta colpm3
            
            
            rts


;*************************************  DISPLAY LISTS 
    

        .align $100                             ; game screen
dlist_game  dta b($70),b($70),b($70)
            dta b($42),a(vram_game_head)

:2          dta b($02)

dl_view
:10         dta b($75),a(vram_game+(#*$100))
            dta b($55),a(vram_game+(11*$100))
            
            dta b($42),a(vram_game_foot)
            
            dta b($41),a(dlist_game)


;*************************************  VIDEO RAM
        .align $400                             

.array      vram_game_head [3*40] .byte = 0
[0] = "chatNoir"
.enda

.array      vram_game_foot [1*40] .byte = 0
[0] = "time"
.enda


        .align $1000                             

.array      vram_game [FIELD_W*FIELD_H] .byte = 1
[$0000] = "O====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$0100] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0200] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0300] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0400] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0500] = "0====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$0600] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0700] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0800] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0900] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0a00] = "+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$0b00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0c00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0d00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0e00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$0f00] = "+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----"
[$1000] = "O====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$1100] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1200] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1300] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1400] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1500] = "0====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$1600] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1700] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1800] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1900] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1a00] = "+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+===="
[$1b00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1c00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1d00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1e00] = "|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    "
[$1f00] = "+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----"
.enda



;*************************************  PMG MEMORY

            .align $800
pmg         .ds 2048